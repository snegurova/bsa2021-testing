import CartParser from './CartParser';

let parser;

beforeEach(() => {
  parser = new CartParser();
  parser.schema = {
    columns: [
      {
        name: 'Product name',
        key: 'name',
        type: parser.ColumnType.STRING,
      },
      {
        name: 'Price',
        key: 'price',
        type: parser.ColumnType.NUMBER_POSITIVE,
      },
      {
        name: 'Quantity',
        key: 'quantity',
        type: parser.ColumnType.NUMBER_POSITIVE,
      },
    ],
  };
});

describe('CartParser - unit tests', () => {
  // Add your unit tests here.
  it('should return line as item object', () => {
    const item = parser.parseLine('Mollis consequat,9.00,2');
    item.id = '3e6def17-5e87-4f27-b6b8-ae78948523a9';
    expect(item).toEqual(
      {
        'id': '3e6def17-5e87-4f27-b6b8-ae78948523a9',
        'name': 'Mollis consequat',
        'price': 9,
        'quantity': 2,
      },
    );
  });
  
  it(`should add error with type \'header\' when headers don\'t match schema`, () => {
    const csvString = 'Product name wrong,Price,Quantity\n';
    const errors = parser.validate(csvString);
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          type: parser.ErrorType.HEADER,
        }),
      ]),
    );
  });
  
  it(`should pass with no error with type \'header\' when headers match schema`, () => {
    const csvString = 'Product name,Price,Quantity\n';
    const errors = parser.validate(csvString);
    if (errors.length === 0) {
      errors.push({});
    }
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          type: parser.ErrorType.HEADER,
        }),
      ]),
    );
  });
  
  it('should add error with type \'row\' when columns number doesn\'t match schema', () => {
    const csvString = 'Product name,Price,Quantity\nMollis consequat,9.00';
    const errors = parser.validate(csvString);
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          type: parser.ErrorType.ROW,
        }),
      ]),
    );
  });
  
  it('should pass with no error with type \'row\' when columns number match schema', () => {
    const csvString = 'Product name,Price,Quantity\nMollis consequat,9.00,2';
    const errors = parser.validate(csvString);
    if (errors.length === 0) {
      errors.push({});
    }
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          type: parser.ErrorType.ROW,
        }),
      ]),
    );
  });
  
  it('should add error with type \'cell\' when cell is empty string', () => {
    const csvString = 'Product name,Price,Quantity\n,9.00,2';
    const errors = parser.validate(csvString);
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          type: parser.ErrorType.CELL,
        }),
      ]),
    );
  });
  
  it('should pass with no error with type \'cell\' when cell has value', () => {
    const csvString = 'Product name,Price,Quantity\nMollis consequat,9.00,2';
    const errors = parser.validate(csvString);
    if (errors.length === 0) {
      errors.push({});
    }
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          type: parser.ErrorType.CELL,
        }),
      ]),
    );
  });
  
  it('should add error with type \'cell\' when cell is not positive number', () => {
    const csvString = 'Product name,Price,Quantity\nMollis consequat,some string,2';
    const errors = parser.validate(csvString);
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          type: parser.ErrorType.CELL,
        }),
      ]),
    );
  });
  
  it('should pass with no error with type \'cell\' when cell is positive number', () => {
    const csvString = 'Product name,Price,Quantity\nMollis consequat,9.00,2';
    const errors = parser.validate(csvString);
    if (errors.length === 0) {
      errors.push({});
    }
    expect(errors).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          type: parser.ErrorType.CELL,
        }),
      ]),
    );
  });
  
  it('should pass with no error when string is valid', () => {
    const csvString = 'Product name,Price,Quantity\nMollis consequat,9.00,2';
    const errors = parser.validate(csvString);
    expect(errors).toEqual(
      expect.arrayContaining([]),
    );
  });
  
});

const fetchData = async () => {
  const url = 'https://bitbucket.org/snegurova/bsa2021-testing/raw/3f94770946f210c873a061084b53d49990fe373e/samples/cart.json';
  const response = await fetch(url);
  return response.json();
};

describe('CartParser - integration test', () => {
  // Add your integration test here.
  
  it('should calculate total price 2', async () => {
    const data = await fetchData();
    expect(parser.calcTotal(data.items)).toBeCloseTo(data.total);
  });
});